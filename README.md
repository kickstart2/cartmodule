### Materials for internal SOLID workshop   

---

Swift playground project with example of Shopping Cart and helper for quick unit tests.

Discussed questions: 
    
    - How to use playground for big projects. 
    - Importing dynamic framework to swift playground + CocoaPods 
    - TDD: where it might go wrong? 
    - Open-closed principle on example of checkout discount strategies

    
    

---

import Foundation

func generateItem() -> SimpleItem {
    let name = "Product"
    let price = 5.5
    let itemID = "123"
    let item = Item(name: name, price: price, itemID: itemID)
    
    return item
}

func generateItem(withID id: String) -> SimpleItem {
    let name = "Product \(id)"
    let price = 15.5
    let itemID = id
    let item = Item(name: name, price: price, itemID: itemID)
    
    return item
}

func generateItem(withID id: String, andPrice price: Double) -> SimpleItem {
    let name = "Product \(id)"
    let itemID = id
    let item = Item(name: name, price: price, itemID: itemID)
    
    return item
}

func testItemGenerator() {
    let item = generateItem()
    XCTAssertEqual(item.getName(), "Product")
    XCTAssertEqual(item.getItemPrice(), 5.5)
    XCTAssertEqual(item.getItemID(), "123")
}
testItemGenerator()

func testAddingRemoving() {
    let cart = ShoppingCart()
    let item = generateItem()
    XCTAssertEqual(cart.getTotalPrice(), 0)
    
    cart.add(item: item)
    XCTAssertNotNil(cart.items[item.getItemID()])
    XCTAssertEqual(cart.items[item.getItemID()]!.getItemID(), "123")
    XCTAssertEqual(cart.getCount(itemID: item.getItemID()), 1)
    XCTAssertEqual(cart.getTotalPrice(), 5.5)
    
    cart.add(item: item)
    XCTAssertEqual(cart.getCount(itemID: item.getItemID()), 2)
    XCTAssertEqual(cart.getTotalPrice(), 11)
    
    let otherItem = generateItem(withID: "222")
    cart.add(item: otherItem)
    XCTAssertEqual(cart.getCount(itemID: item.getItemID()), 2)
    XCTAssertEqual(cart.getCount(itemID: otherItem.getItemID()), 1)
    
    cart.remove(item: item)
    XCTAssertEqual(cart.getCount(itemID: item.getItemID()), 1)
    cart.remove(item: item)
    XCTAssertEqual(cart.getCount(itemID: item.getItemID()), 0)
    XCTAssertNil(cart.items[item.getItemID()])
    cart.remove(item: item)
    
    XCTAssertEqual(cart.getCount(itemID: item.getItemID()), 0)
    XCTAssertEqual(cart.getTotalPrice(), 15.5)
    cart.add(item: item, count: 1000)
    XCTAssertEqual(cart.getCount(itemID: item.getItemID()), 1000)
    XCTAssertEqual(cart.getTotalPrice(), 5515.5)
}
testAddingRemoving()

func testDiscountForCount() {
    let cart = ShoppingCart()
    let item = generateItem()
    cart.add(item: item)
    XCTAssertEqual(cart.getTotalPrice(), 5.5)
    
    let discount = DiscountForCount(item: item, minCount: 3, discount: 2/3)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 0)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 0)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 11)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 14.67)
}
testDiscountForCount()

func testDiscountMforN() {
    let cart = ShoppingCart()
    let item = generateItem()
    cart.add(item: item)
    XCTAssertEqual(cart.getTotalPrice(), 5.5)
    
    let discount = DiscountMforN(item: item, minCount: 3, discount: 1/3)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 0)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 0)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 5.5)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 5.5)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 5.5)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 11)
}
testDiscountMforN()

func testDiscountBOGOF() {
    let cart = ShoppingCart()
    let item = generateItem()
    cart.add(item: item)
    XCTAssertEqual(cart.getTotalPrice(), 5.5)
    
    let discount = DiscountBOGOF(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 0)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 5.5)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 5.5)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 11)
    cart.add(item: item)
    XCTAssertEqual(discount.calculateDeductionAmount(cart), 11)
}
testDiscountBOGOF()

func testCheckout() {
    let cart = ShoppingCart()
    let itemOne = generateItem(withID: "1", andPrice: 1)
    let itemTwo = generateItem(withID: "2", andPrice: 2)
    let itemThree = generateItem(withID: "3", andPrice: 3)
    cart.add(item: itemOne)
    cart.add(item: itemTwo)
    cart.add(item: itemThree)
    
    let discountBOGOF:CartReduction = DiscountBOGOF(item: itemTwo)
    let discount3for2:CartReduction = DiscountMforN(item: itemOne, minCount: 3, discount: 1/3)
    let checkout:Checkout = ShopCheckout(discounts: [discountBOGOF, discount3for2])
    XCTAssertEqual(checkout.calculateTotal(cart: cart), 6)
    cart.add(item: itemTwo)
    XCTAssertEqual(checkout.calculateTotal(cart: cart), 6)
    cart.add(item: itemThree)
    XCTAssertEqual(checkout.calculateTotal(cart: cart), 9)
    cart.add(item: itemOne)
    XCTAssertEqual(checkout.calculateTotal(cart: cart), 10)
    cart.add(item: itemOne)
    XCTAssertEqual(checkout.calculateTotal(cart: cart), 10)
    cart.add(item: itemTwo, count: 2)
    XCTAssertEqual(checkout.calculateTotal(cart: cart), 12)
}
testCheckout()

protocol Cart {
    var items: [String: SimpleItem] { get }
    func add(item: SimpleItem)
    func add(item: SimpleItem, count: Int)
    func remove(item: SimpleItem)
    func getCount(itemID: String) -> Int
    func getTotalPrice() -> Double
}

protocol SimpleItem {
    func getName() -> String
    func getItemID() -> String
    func getItemPrice() -> Double
}

protocol CartReduction {
    func calculateDeductionAmount(_ cart:Cart) -> Double
}

protocol Checkout {
    func calculateTotal(cart:Cart) -> Double
}

struct ShopCheckout: Checkout {
    let discounts:[CartReduction]
    func calculateTotal(cart: Cart) -> Double {
        var totalPrice = cart.getTotalPrice()
        for discount in discounts {
            let deductionValue = discount.calculateDeductionAmount(cart)
            totalPrice -= deductionValue
        }
        return totalPrice.rounded(toPlaces: 2)
    }
}

struct DiscountForCount: CartReduction {
    let item:SimpleItem
    let minCount:Int
    let discount:Double
    
    func calculateDeductionAmount(_ cart:Cart) -> Double {
        let count = cart.getCount(itemID: item.getItemID())
        let deduction:Double = Double(count / self.minCount) * discount
        return (cart.getTotalPrice() * deduction).rounded(toPlaces: 2)
    }
}

struct DiscountMforN: CartReduction {
    let item:SimpleItem
    let minCount:Int
    let discount:Double
     
    func calculateDeductionAmount(_ cart:Cart) -> Double {
        let count = cart.getCount(itemID: item.getItemID())
        let numOfDiscounts = floor(Double(count / self.minCount))
        return (item.getItemPrice() * Double(minCount) * discount * numOfDiscounts).rounded(toPlaces: 2)
    }
}

struct DiscountBOGOF: CartReduction {
    let item:SimpleItem
    func calculateDeductionAmount(_ cart:Cart) -> Double {
        return DiscountMforN(item: self.item, minCount: 2, discount: 1/2).calculateDeductionAmount(cart)
    }
}

class ShoppingCart: Cart {
    var items: [String: SimpleItem] = [:]
    var counts: [String: Int] = [:]
    
    func add(item: SimpleItem) {
        self.add(item: item, count: 1)
    }
    
    func add(item: SimpleItem, count: Int) {
        let itemID = item.getItemID()
        if (self.hasItem(itemID)) {
            self.counts[itemID] = self.counts[itemID]! + count
            return
        }
        self.counts[itemID] = count
        self.items[itemID] = item
    }
    
    func remove(item: SimpleItem) {
        let itemID = item.getItemID()
        if (!self.hasItem(itemID)) {
            return
        }
        self.counts[itemID]! -= 1
        if (self.counts[itemID]! == 0) {
            self.counts.removeValue(forKey: item.getItemID())
            self.items.removeValue(forKey: item.getItemID())
        }
    }
    
    func getCount(itemID: String) -> Int {
        if (self.hasItem(itemID)) {
            return self.counts[itemID]!
        }
        return 0
    }
    
    func getTotalPrice() -> Double {
        var totalPrice = 0.0
        for item in self.items.values {
            totalPrice += item.getItemPrice() * Double(self.getCount(itemID: item.getItemID()))
        }
        return totalPrice.rounded(toPlaces: 2)
    }
    
    func hasItem(_ itemID: String) -> Bool {
        return self.items[itemID] != nil
    } 
}



struct Item: SimpleItem {
    let name: String
    let price: Double
    let itemID: String
    
    func getName() -> String {
        return self.name
    }
    
    func getItemID() -> String {
        return self.itemID
    }
    
    func getItemPrice() -> Double {
        return self.price
    }
}

extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
